#!/usr/bin/python

import socket
import re
import random
import sys

TCP_IP = '127.0.0.1'
TCP_PORT = 1337

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

s.recv(4096)

for n in range(0, 100):
	msg = 'set ' + str(n) + ' val_' + str(n)
	print msg
	s.send(msg + '\n')
	print s.recv(4096)

for n in range(0, 100):
	msg = 'set ' + str(n)
	print msg
	s.send(msg + '\n')
	print s.recv(4096)

s.send('show\n')
print 'show'
print s.recv(4096)

s.close()
